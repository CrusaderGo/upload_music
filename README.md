# README

* Ubuntu: `sudo apt-get install libav-tools`. About another OS, search by key: `avconv`
* `bundle install`
* `rake db:migrate`
* `rails s`
* In another tab `curl -v -F "artist=Artist name" -F "song=Song name" -F "file=@path/to/file.mp3" http://localhost:3000/api/v1/audios`
