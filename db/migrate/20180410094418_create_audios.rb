class CreateAudios < ActiveRecord::Migration[5.1]
  def change
    create_table :audios do |t|
      t.string :artist
      t.string :song
      t.integer :filesize
      t.string :file

      t.timestamps
    end
  end
end
